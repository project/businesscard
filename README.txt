Module: Business Card
Author: Mike Carter <mike@ixis.co.uk>


Description
===========
Provides the glue to build your own contacts directory using Flexinode.module


Requirements
============
You will need to know how to use Flexinode before you can create your contact directory.

* Drupal 4.6 installation
* flexinode.module activated
* menu.module enabled


Installation
============
* Copy the 'businesscard' module directory in to your Drupal
modules directory as usual.
* active the module in ?q=admin/modules


Settings
========
You must first define a 'contact card' content type using flexinode. A guide on
creating new content types is available from: http://drupal.org/handbook/modules/flexinode

Your 'contact card' content type would normally include fields such as:
      surname, firstname, telephone number, mobile number, company, position etc.

Browse to the admin settings at ?q=admin/settings/businesscard

You need to tell businesscard.module what content type you created should
represent the 'contact card'.  Use the drop down menu to select the content type you just made.

Click the 'Save configuration' button.

You will now see that the 'Available variables' list has updated to show all the fields
you defined in your 'contact card' form earlier. For example:

      %firstname, %surname, %company

When contact cards are listed in the directory you can define the title by gluing together
fields from the form. Enter a 'Card Title Format' in the corresponding box.

Click the 'Save configuration' button to finalise your configuration.


Usage
=====
A new suggested menu item is provided by businesscard.module - this can be enabled via
the menu.module interface at ?q=admin/menu

The main Business Card page is ?q=businesscard and provides two tabs to view either
the A-Z list or the search form.

Contact Directory Listing
Accessed by ?q=businesscard/list
Lists all contacts, 20 per page. You can sort contacts via the column headings.

Contact Search
Accessed by ?q=businesscard/search
Allows users to search the 'contact card' nodes specifically.
The normal Drupal search will also find these contact card entries.

Add a Contact
Accessed by ?q=businesscard/add
Displays the usual Drupal node add form for a contact card.


Security
========
To restrict who can see your 'contact card' nodes I suggest using the Simple_Access module.
Available from http://drupal.org/project/simple_access

